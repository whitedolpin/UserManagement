﻿namespace UserManagementFA2023.Models.DTO
{
    public class UserDTO
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public DateTime DateOfBird { get; set; }
        public List<string> Classes { get; set; }
        public string Gender { get; set; }  
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Status { get; set; }
    }
}
