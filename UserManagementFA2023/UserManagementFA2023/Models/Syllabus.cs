﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using UserManagementFA2023.Models.DTO;
using UserManagementFA2023.Models.Models.DTO;

namespace UserManagementFA2023.Models
{
    public class Syllabus
    {
        public int SyllabusId { get; set; }
        public string? SyllabusTittle { get; set; }
        public string? SyllabusCode { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string? CreatedByUserId { get; set; }
        public UserDTO? User { get; set; }
        public int? Duration { get; set; }
        public string? OutPutStandard { get; set; }
        public int? StatusId { get; set; }
        public Status? Status { get; set; }
        public string? SyllabusUrl { get; set; }
        public string? SyllabusLocalPath { get; set; }
    }
}
