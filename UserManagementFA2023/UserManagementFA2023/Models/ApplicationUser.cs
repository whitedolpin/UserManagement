﻿using Microsoft.AspNetCore.Identity;

namespace UserManagementFA2023.Models
{
    public class ApplicationUser : IdentityUser
    {
        public string Name { get; set; }
        public List<string> Classes { get; set; }
        public DateTime DateOfBird { get; set; }
        public string Gender { get; set; }
        public string Status { get; set; }
    }
}
