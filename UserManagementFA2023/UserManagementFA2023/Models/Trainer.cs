﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace UserManagementFA2023.Models
{
    public class Trainer
    {
        [Key]
        public int TrainerId { get; set; }
        [Key]
        public int ClassId { get; set; }
        [NotMapped]
        public Class? Class { get; set; }    
    }
}
