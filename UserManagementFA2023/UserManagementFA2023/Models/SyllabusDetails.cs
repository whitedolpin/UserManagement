﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using UserManagementFA2023.Models.Models.DTO;

namespace UserManagementFA2023.Models
{
    public class SyllabusDetails
    {
        public int SyllabusDetailId { get; set; }
        public int? SyllabusId { get; private set; }
        public Syllabus? Syllabus { get; set; }
        public DateTime? LastModifyDate { get; set; }
        public string? Description { get; set; }
        public string? RequirementDescription { get; set; }
        public int? LessonId { get; set; }
        public Lesson? Lesson { get; set; }
        public int? LevelId { get; set; }
        public Level? Level { get; set; }
        public int? AtendeeNumber { get; set; }
    }
}
