﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace UserManagementFA2023.Models
{
    public class Timeline
    {
        [Key]
        public int TimeLineId { get; set; } 
        public int SyllabusDetailId { get; set; }
        [ForeignKey("SyllabusDetailId")]
        public SyllabusDetails? SyllabusDetails { get; set;}
        public string Description { get; set; } 
        public string UnitName { get; set; }    
        public string Type { get; set; }    
        public int Day { get; set; }
    }
}
