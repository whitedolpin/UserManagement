﻿using System.ComponentModel.DataAnnotations;

namespace UserManagementFA2023.Models
{
    public class Lesson
    {
        [Key]
        public int LessonId { get; set; } 
        public string LessonName { get; set; }  
        public string Description { get; set; } 
        public string Method { get; set; }    
        public int TraningTime { get; set; }    
        public string UnitName { get; set; }    
        public string LessonMaterial { get; set; }  
    }
}
