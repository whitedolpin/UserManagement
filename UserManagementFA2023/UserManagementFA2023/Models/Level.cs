﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace UserManagementFA2023.Models.Models.DTO    
{
    public class Level
    {
        public int LevelId { get; set; }
        public string? LevelName { get; set; }
        public int? ClassId { get; set; }
        public Class? Class { get; set; }    
    }
}
