﻿using System.ComponentModel.DataAnnotations;

namespace UserManagementFA2023.Models.Models.DTO
{
    public class Status
    {
        public int StatusId { get; set; } 
        public bool? IsActive { get; set; }  
     
    }
}
