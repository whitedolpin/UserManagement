﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace UserManagementFA2023.Models
{
    public class Attendee
    {
        [Key]
        public int AttendeeId { get; set; }
        [Key]
        public int ClassId { get; set; }
        [NotMapped]
        public List<Class>? Class { get; set; }
    }
}
