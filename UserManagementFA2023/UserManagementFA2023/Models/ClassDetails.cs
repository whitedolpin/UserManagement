﻿using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace UserManagementFA2023.Models
{
    public class ClassDetails
    {
        [Key]
        public int ClassDetailId { get; set; }
        [ForeignKey("ClassId")]
        public int ClassId { get; set; }
        public Class? Class { get; set; }    
        public DateTime ClassTime { get; set; }  
        public DateTime DateFrom { get; set; }  
        public DateTime DateTo { get; set; }    
        public string Location { get; set; }    
        public string Method { get; set; }    
        public int LocationId { get; set; }
        [ForeignKey("Id")]
        public int UserId { get; set; }
        public IdentityUser User { get; set; }    
    }
}
