﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserManagementFA2023.DataAccess.Data;
using UserManagementFA2023.DataAccess.Repository.IRepository;
using UserManagementFA2023.Models.Models;
using UserManagementFA2023.Models.Models.DTO;

namespace UserManagementFA2023.DataAccess.Repository
{
    public class Repository<T> : IRepository<T> where T : class
    {
        private readonly AppDbContext _db;
        private ResponseDTO _response;
        private IMapper _mapper;
        private readonly DbSet<T> _set;
        public Repository(AppDbContext db) {
            _db = db;
            _response = new ResponseDTO();   
            this._set = _db.Set<T>();       
        }

        public async Task<ResponseDTO> GetAll()
        {
            try
            {
                
                _response.Result = _set.ToList();
            }
            catch (Exception ex)
            {
                _response.Message = ex.Message;
                _response.IsSuccess = false;
            }
            return _response;
        }

        public async Task<ResponseDTO> GetById(int id)
        {
            try
            {
                _response.Result = _set.Find(id);
            } catch (Exception ex)
            {
                _response.Message = ex.Message;
                _response.IsSuccess = false;
            }
            return _response;
        }

        public async Task<ResponseDTO> Create(T entity)
        {
            try
            {
                _set.Add(entity);
                _response.Message = "Add successfully";
            } catch (Exception ex)
            {
                _response.Message = ex.Message;
                _response.IsSuccess = false;
            }
            return _response;   
        }

        public async Task<ResponseDTO> Update(T entity)
        {
            try
            {
                _set.Update(entity);
                _response.Message = "Updated successfully";
            } catch (Exception ex)
            {
                _response.Message = ex.Message;
                _response.IsSuccess = false;
            }
            return _response;   
        }

        public async Task<ResponseDTO> DeleteById(int id)
        {
            try
            {
               var obj = _set.Find(id);
               _set.Remove(obj);    
            } catch (Exception ex)
            {
                _response.Message = ex.Message;
                _response.IsSuccess = false;
            }
            return _response;
        }
    }
}
