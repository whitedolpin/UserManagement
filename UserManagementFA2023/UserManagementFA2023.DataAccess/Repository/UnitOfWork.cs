﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserManagementFA2023.DataAccess.Data;
using UserManagementFA2023.DataAccess.Repository.IRepository;

namespace UserManagementFA2023.DataAccess.Repository
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly AppDbContext _db;
        public ISyllabusRepository syllabusRepository { get; } 

        public UnitOfWork(AppDbContext db)
        {
            _db = db;
            syllabusRepository = new SyllabusRepository(_db);

        }
        public void Save()
        {
            _db.SaveChanges();
        }
    }
}
