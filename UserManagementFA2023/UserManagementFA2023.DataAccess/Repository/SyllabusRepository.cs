﻿using UserManagementFA2023.DataAccess.Data;
using UserManagementFA2023.DataAccess.Repository.IRepository;
using UserManagementFA2023.Models.Models;
using UserManagementFA2023.Models.Models.DTO;

namespace UserManagementFA2023.DataAccess.Repository
{
    public class SyllabusRepository : Repository<Syllabus>, ISyllabusRepository
    {
        private  AppDbContext _db;
        public ISyllabusRepository syllabus { get; private set; }

        public SyllabusRepository(AppDbContext db) : base(db)
        {
            _db = db;
        }
        public Task<ResponseDTO> ImportSyllabus()
        {
            throw new NotImplementedException();
        }
    }
}