﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserManagementFA2023.DataAccess.Repository.IRepository
{
    public interface IUnitOfWork 
    {
        public ISyllabusRepository syllabusRepository { get; } 
        public void Save();
    }
}
