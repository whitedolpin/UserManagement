﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserManagementFA2023.Models.Models;
using UserManagementFA2023.Models.Models.DTO;

namespace UserManagementFA2023.DataAccess.Repository.IRepository
{
    public interface IRepository<T> where T : class 
    {
        Task<ResponseDTO> GetAll();
        Task<ResponseDTO> GetById (int id);
        Task<ResponseDTO> Create (T entity);    
        Task<ResponseDTO> Update(T entity);
        Task<ResponseDTO> DeleteById (int id);
        
    }
}
