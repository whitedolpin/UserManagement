﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using UserManagementFA2023.Models.Models;

namespace UserManagementFA2023.DataAccess.Data
{
    public class AppDbContext : IdentityDbContext<ApplicationUser>
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {

        }

        public DbSet<ApplicationUser> ApplicationUsers { get; set; }
        public DbSet<Syllabus> Syllabus { get; set; }
        public DbSet<SyllabusDetailsDTO> SyllabusDetails { get; set; }
        public DbSet<ClassDTO> Class { get; set; }
        public DbSet<ClassDetailsDTO> ClassDetails { get; set; }
        public DbSet<LessonDTO> Lessons { get; set; }
        public DbSet<Location> Locations { get; set; }
        public DbSet<TimelineDTO> Timelines { get; set; }
        public DbSet<Attendence> Attendences { get; set; }
        public DbSet<DayDTO> Days { get; set; }
        public DbSet<LevelDTO> Levels { get; set; }
        public DbSet<UnitDTO> Units { get; set; }
        public DbSet<MaterialTypeDTO> MaterialTypes { get; set; }
        public DbSet<StatusDTO> Statuses { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<ClassDetailsDTO>().HasKey(i => new
            {
                i.ClassId,
                i.UserId
            });
            //        modelBuilder.Entity<ClassDetails>()
            //.HasOne(cd => cd.Location)
            //.WithMany()
            //.HasForeignKey(cd => cd.LocationId)
            //.OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Attendence>().HasKey(i => new
            {
                i.ClassId,
                i.UnitId
            });
            //        modelBuilder.Entity<Timeline>()
            //        .HasOne(t => t.Unit)
            //         .WithMany()
            //         .HasForeignKey(t => t.UnitId)
            //           .OnDelete(DeleteBehavior.Restrict);

        }

    }
}