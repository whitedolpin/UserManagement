﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace UserManagementFA2023.Models.Models
{
    public class Syllabus
    {
        [Key]
        public int SyllabusId { get; set; }
        public string? SyllabusTittle { get; set; }
        public string? SyllabusCode { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string? CreatedByUserId { get; set; }
        [ForeignKey("Id")]
        public ApplicationUser? User { get; set; }   
        public int? Duration { get; set; }   
        public string? OutPutStandard { get; set; }  
        public int? StatusId { get; set; }
        [ForeignKey("StatusId")]
        public StatusDTO? Status { get; set; }
    }
}
