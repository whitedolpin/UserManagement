﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace UserManagementFA2023.Models.Models
{
    public class LevelDTO
    {
        [Key]
        public int LevelId { get; set; }
        public string? LevelName { get; set; }
        public int? ClassId { get; set; }
        [ForeignKey("ClassId")]
        public ClassDTO? Class { get; set; }    
    }
}
