﻿using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations.Schema;

namespace UserManagementFA2023.Models.Models.DTO    
{
    public class UserDTO
    {
        public string Id { get; set; }  
        public string Name { get; set; }    
        public string Email { get; set; }   
        public string PhoneNumber { get; set; }    
       
    }
}
