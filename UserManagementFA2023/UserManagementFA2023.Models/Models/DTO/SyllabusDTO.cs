﻿using Microsoft.AspNetCore.Http;
using UserManagementFA2023.Utility;

namespace UserManagementFA2023.Models.Models.DTO
{
    public class SyllabusDTO
    {
        public int SyllabusId { get; set; }
        public string? SyllabusTittle { get; set; }
        public string? SyllabusCode { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string? CreatedByUserId { get; set; }
        public UserDTO? User { get; set; }
        public int? Duration { get; set; }
        public string? OutPutStandard { get; set; }
        public int? StatusId { get; set; }
        public StatusDTO? Status { get; set; }
        public string? SyllabusUrl { get; set; }
        public string? SyllabusLocalPath { get; set; }  

    }
}
