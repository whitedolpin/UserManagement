﻿namespace UserManagementFA2023.Models.Models.DTO
{
    public class LoginResponseDTO
    {
        public UserDTO User { get; set; }
        public string Token { get; set; }   
    }
}
