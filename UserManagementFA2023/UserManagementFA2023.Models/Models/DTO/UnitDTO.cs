﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace UserManagementFA2023.Models.Models.DTO
{
    public class UnitDTO
    {
        public int UnitId { get; set; }   
        public string? UnitName { get; set; }        
        public int? DayId { get; set; }
        public DayDTO? Day { get; set; }    
    }
}
