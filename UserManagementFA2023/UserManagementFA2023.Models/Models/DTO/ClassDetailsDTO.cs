﻿using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace UserManagementFA2023.Models.Models.DTO
{
    public class ClassDetailsDTO
    {
        public int ClassId { get; set; }
        public ClassDTO? Class { get; set; }
        public string? UserId { get; set; }
        public UserDTO? User { get; set; }
        public DateTime? ClassTime { get; set; }
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
        public string Method { get; set; }
      
    }
}
