﻿using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace UserManagementFA2023.Models.Models
{
    public class ClassDetailsDTO
    {
        [Key, Column(Order =1)] 
        public int ClassId { get; set; }
        [ForeignKey("ClassId")]
        public ClassDTO? Class { get; set; }
        [Key, Column(Order = 2)]

        public string? UserId { get; set; }
        [ForeignKey("Id")]
        public ApplicationUser? User { get; set; }
        public DateTime? ClassTime { get; set; }
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
       
        public string Method { get; set; }
      
    }
}
