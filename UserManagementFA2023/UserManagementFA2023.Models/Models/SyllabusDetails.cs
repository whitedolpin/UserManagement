﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace UserManagementFA2023.Models.Models
{
    public class SyllabusDetailsDTO
    {
        [Key]
        public int SyllabusDetailId { get; set; }
        public int? SyllabusId { get; private set; }
        [ForeignKey("SyllabusId")]
        public Syllabus? Syllabus { get; set; }  
        public DateTime? LastModifyDate { get; set; }
        public string? Description { get; set;} 
       
        public string? RequirementDescription { get; set;}
        public int? LessonId { get; set;}
        [ForeignKey("LessonId")]
        public LessonDTO? Lesson { get; set; } 
        public int? LevelId { get; set;}
        [ForeignKey("LevelId")]
        public LevelDTO? Level { get; set;}  
        public int? AtendeeNumber { get; set;}   
    }
}
