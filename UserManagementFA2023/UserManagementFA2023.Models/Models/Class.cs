﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace UserManagementFA2023.Models.Models
{
    public class ClassDTO 
    {
        [Key]
        public int ClassId { get; set; } 
        public string? ClassName { get; set; }   
        public DateTime? DateCreate { get; set; }
        public int? LocationId { get; set; }
        [ForeignKey("LocationId")]
        public Location? Location { get; set; }  
        public string? FSU { get; set; }
        public string? Status { get; set; }
        public string? UserId { get; set; }
        [ForeignKey("Id")]
        public ApplicationUser? User { get; set; }
        public int? NumberOfAttendee { get; set; }   
    }
}
