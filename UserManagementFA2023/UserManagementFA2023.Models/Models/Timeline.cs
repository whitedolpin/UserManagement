﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace UserManagementFA2023.Models.Models
{
    public class TimelineDTO
    {
        [Key]
        public int TimeLineId { get; set; } 
        public int? SyllabusDetailId { get; set; }
        [ForeignKey("SyllabusDetailId")]
        public SyllabusDetailsDTO? SyllabusDetails { get; set;}
        public string? Description { get; set; } 
        public int? UnitId { get; set; }
        [ForeignKey("UnitId")]
        public UnitDTO? Unit { get; set; }  

        public int? MatertialTypeId { get; set; }
        [ForeignKey("MatertialTypeId")]    
        public MaterialTypeDTO? MaterialType { get; set; }  
        
    }
}
