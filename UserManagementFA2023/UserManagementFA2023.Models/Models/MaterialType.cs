﻿using System.ComponentModel.DataAnnotations;

namespace UserManagementFA2023.Models.Models
{
    public class MaterialTypeDTO
    {
        [Key]
        public int MaterialTypeId { get; set; } 
        public string? MaterialTypeName { get; set; }
    }
}
