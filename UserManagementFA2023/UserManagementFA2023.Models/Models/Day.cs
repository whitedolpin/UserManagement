﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace UserManagementFA2023.Models.Models
{
    public class DayDTO
    {
        [Key]
        public int DayId { get; set; } 
        public DateTime? DateTime { get; set; }
        public int? TimelineId { get; set; }
        public TimelineDTO? Timeline { get; set; }
        
    }
}
