﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserManagementFA2023.Models.Models;
using UserManagementFA2023.Models.Models.DTO;

namespace UserManagementFA2023.API
{

    public class MappingConfig
    {
        public static MapperConfiguration RegisterMaps()
        {
            var mappingCongif = new MapperConfiguration(config =>
            {
                config.CreateMap<Syllabus, SyllabusDTO>().ReverseMap(); 
            });
            return mappingCongif;
        }
    }

}
