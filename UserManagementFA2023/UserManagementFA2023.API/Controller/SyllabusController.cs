﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Stripe;
using UserManagementFA2023.DataAccess.Repository.IRepository;
using UserManagementFA2023.Models.Models;
using UserManagementFA2023.Models.Models.DTO;

namespace UserManagementFA2023.API.Controller
{
    [Route("api/syllabus")]
    [ApiController]
    public class SyllabusController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ResponseDTO _response;
        private IMapper _mapper;
        public SyllabusController(IUnitOfWork unit, IMapper mapper)
        {
            _unitOfWork = unit;
            _response = new ResponseDTO();
            _mapper = mapper;
        }
        [HttpGet]
        public async Task<ResponseDTO> GetAll() {
            return await _unitOfWork.syllabusRepository.GetAll();
        }
        [HttpPost]
        public async Task<ResponseDTO> AddSyllabus(Syllabus syllabus)
        {
            var obj = _mapper.Map<Syllabus>(syllabus);
            return await _unitOfWork.syllabusRepository.Create(obj);
        }
    }
}
